const express = require('express');
const router = express.Router();

const user_controller = require('../controllers/user.controller');


// User routes
router.route('/')
    .get(user_controller.index);

router.route('/add')
    .post(user_controller.new);

router.route('/get')
    .get(user_controller.view);



// Export API routes
module.exports = router;