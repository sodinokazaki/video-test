const express = require('express');
const router = express.Router();

const watch_controller = require('../controllers/watch.controller');


// User routes
router.route('/')
    .get(watch_controller.update);


// Export API routes
module.exports = router;