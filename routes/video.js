const express = require('express');
const router = express.Router();

const video_controller = require('../controllers/video.controller');


// Contact routes
router.route('/')
    .get(video_controller.index)

router.route('/add')
    .post(video_controller.new);

router.route('/get')
    .get(video_controller.view)

// Export API routes
module.exports = router;