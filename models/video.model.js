const mongoose = require('mongoose');

// Setup schema
const videoSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    video_id: {
        type: String,
        required: true
    }
});

// Export User model
const Video = mongoose.model('Video', videoSchema);

module.exports = Video;

module.exports.get = function (callback, limit) {
    Video.find(callback).limit(limit);
}