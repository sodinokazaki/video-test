const mongoose = require('mongoose');

// Setup schema
const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    open_videos: {
        type: Array,
    }
});

// Export User model
const User = mongoose.model('User', userSchema);

module.exports = User;

module.exports.get = function (callback, limit) {
    User.find(callback).limit(limit);
}