// Import User model
User = require('../models/user.model');


// Handle view User info


// Handle update User info
exports.update = function (req, res) {

    User.findById(req.cookies.user_id, (err, user) => {

        if (err) {
            res.send(err);
        }

        if (user.open_videos.length === 3) {
            res.send('Max no of videos open');
        } else {
            user.open_videos.push(req.query.video_id);
            user.save(function (err) {
                if (err)
                    res.json(err);
                res.json({
                    message: 'Video id',
                    data: user
                });
            });
        }

        // save the User and check for errors

    });
};


