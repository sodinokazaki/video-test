// Import User model
User = require('../models/user.model');

// Handle index actions
exports.index = function (req, res) {
    User.get(function (err, user) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Users retrieved successfully",
            data: user
        });
    });
};

// Handle create User actions
exports.new = function (req, res) {
    var user = new User();
    user.name = req.body.name ? req.body.name : user.name;
    user.email = req.body.email;
    user.open_videos = [];

    // save the User and check for errors
    user.save(function (err) {
        // if (err)
        //     res.json(err);

        res.json({
            message: 'New User created!',
            data: User
        });
    });
};


// Handle view User info
exports.view = function (req, res) {
    User.findById(req.query.user_id, function (err, user) {
        if (err)
            res.send(err);
        res.json({
            message: 'User details loading..',
            data: user
        });
    });
};

// Handle update User info
exports.update = function (req, res) {

    User.findById(req.params.user_id, function (err, user) {
        if (err)
            res.send(err);

        user.name = req.body.name ? req.body.name : user.name;
        user.email = req.body.email;
        user.open_videos = [];

        // save the User and check for errors
       user.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'User Info updated',
                data: user
            });
        });
    });
};


// Handle delete User
exports.delete = function (req, res) {
    User.remove({
        _id: req.params.user_id
    }, function (err, user) {
        if (err)
            res.send(err);

        res.json({
            status: "success",
            message: 'User deleted'
        });
    });
};