// Import Video model
Video = require('../models/video.model');

// Handle index actions
exports.index = function (req, res) {
    Video.get(function (err, videos) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Video retrieved successfully",
            data: videos
        });
    });
};

// Handle create Video actions
exports.new = function (req, res) {
    var video = new Video();
    video.name = req.body.name ? req.body.name : video.name;
    video.video_id = req.body.video_id;

    // save the Video and check for errors
    video.save(function (err) {
        // if (err)
        //     res.json(err);

        res.json({
            message: 'New Video created!',
            data: video
        });
    });
};


// Handle view Video info
exports.view = function (req, res) {
    Video.findById(req.query.video_id, function (err, video) {
        if (err)
            res.send(err);
        res.json({
            message: 'Video details loading..',
            data: video
        });
    });
};

// Handle update Video info
exports.update = function (req, res) {

    Video.findById(req.params.Video_id, function (err, video) {
        if (err)
            res.send(err);

        video.name = req.body.name ? req.body.name : video.name;
        video.id = req.body.id? req.body.id : video.id;

        // save the Video and check for errors
        Video.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Video Info updated',
                data: video
            });
        });
    });
};


// Handle delete Video
exports.delete = function (req, res) {
    Video.remove({
        _id: req.params.Video_id
    }, function (err, Video) {
        if (err)
            res.send(err);

        res.json({
            status: "success",
            message: 'Video deleted'
        });
    });
};